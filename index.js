const requestJson = require('request-json');

exports.handler = function(event,context,callback){
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    console.log(event)
    var restriccion = (user.tipo==0) ? ', "owner": "' + user._id + '" ' : ""
    var query = 'account?q={"_id": '+ JSON.stringify(event.pathParameters.accountid) + restriccion + '}&f={"movimientos": 1, "_id" : 0}&' + apikey
    
    var httpClient = requestJson.createClient(db);
    httpClient.get(query,
        function(err,resMLab,body){
            if(err){
                var response = {
                    "statusCode" : 501,
                    "headers" : {"Access-Control-Allow-Origin": "*" },                    
                    "body": '{"msg" : "No puedes acceder a esos datos"}',
                }
            } else {
                if(body.length == 1){body = body[0]}
                
                if(!body.movimientos){
                   var response = { 
                        "statusCode": 403,
                        "headers" : {"Access-Control-Allow-Origin": "*" },                    
                        "body": '{"msg" : "No hay movimientos que tu puedas ver"}',
                        "isBase64Encoded": false
                        };                
                    return callback(null,response); 
                } else if (event.pathParameters && event.pathParameters.movimientoid){
                    var res = body.movimientos.find(function (movimientos) { return movimientos.id === event.pathParameters.movimientoid; }); 
                } else if(event.resource == '/cuentas/{accountid}/saldo' || (event.queryStringParameters && event.queryStringParameters.saldo)){
                    var saldo=0;
                    console.log("whatch my " + JSON.stringify(body.movimientos))
                    var movimientos = body.movimientos
                    movimientos.forEach(function(movimiento) {
                      console.log(movimiento);
                      if(movimiento.tipo == 1){
                          saldo = saldo + movimiento.importe;
                          } else {
                            saldo = saldo - movimiento.importe;
                          }
                    });
                    res= {"saldo" : saldo.toFixed(2) }
                } else res = body.movimientos
                var response = { 
                    "statusCode": 200,
                    "headers" : {"Access-Control-Allow-Origin": "*" },                    
                    "body": JSON.stringify(res),
                    "isBase64Encoded": false
                    };
                }
            callback(null,response); 
        })
};